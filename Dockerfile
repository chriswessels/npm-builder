FROM docker:19.03.2-dind

ENV TERRAFORM_VERSION=0.11.11

RUN apk add --no-cache \
    bash \
    curl \
    docker-compose \
    git \
    g++ \
    jq \
    make \
    nodejs=10.16.3-r0 \
    npm=10.16.3-r0 \
    python \
    unzip \
    zip

RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/testing \
    aws-cli

WORKDIR /usr/local/bin

RUN curl -o /tmp/terraform.zip "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" && unzip /tmp/terraform.zip && chmod +x /usr/local/bin/terraform && rm /tmp/terraform.zip

WORKDIR /root

RUN mkdir -p .ssh && echo "StrictHostKeyChecking no" > .ssh/config
RUN npm install -g serverless npm-check-updates
